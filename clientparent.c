#include <stdio.h>
#include <stdlib.h>
#include <netdb.h>
#include <signal.h>
#include <wait.h>
#define MAXLINE 1024
int M=1;

static void sigchld_hdl (int sig)
{
	pid_t pid;
	/* Wait for all dead processes.
	 * We use a non-blocking call to be sure this signal handler will not
	 * block if a child was cleaned up in another part of the program. */
	while ( (pid =waitpid(-1, NULL, WNOHANG)) > 0) {
		printf("CHILD %d terminated\n",pid);
		M = 0;
	}
}

static void sigpipe_hdl (int sig)
{

		printf("pipe broken\n");
}


int main(int argc, char **argv)
{

	char *ch,inputbuff[1024], *ptr, **pptr;
	int pid,rc, selectedvalue, maxfd;
	int pipeFD[2];
	char buffer[MAXLINE], buf[MAXLINE];
	fd_set setallfd;
	struct sigaction act;

	char str [INET_ADDRSTRLEN];
	unsigned int ip;
	struct hostent *hptr;
	struct sockaddr_in sa;
	int result;
	char *ipaddress;
	if(argc == 2) {
		ptr = *++argv;
	result = inet_pton(AF_INET, ptr, &(sa.sin_addr));
	if (result){
		printf("valid");
		ip = inet_addr(ptr);

		if(hptr = gethostbyaddr((char *)&ip,4,AF_INET)){
		printf("hostname is %s\n", hptr->h_name);
		ipaddress = ptr;
		}
		else{
			printf("no host found");
			return 0;
		}
	}

	else if((hptr = gethostbyname(ptr))){
		switch(hptr->h_addrtype){
			case AF_INET: pptr = hptr->h_addr_list;
				inet_ntop(hptr->h_addrtype, *pptr,str,sizeof(str));
	printf("ip address is %s\n",str);
	ipaddress = str;
	break;
	default:
	printf("unknown address");break;
	}
	}
	else{printf("Invalid arguments");
		return 0;
	}}
	else{
		printf("Invalid number of arguments");
		return 0;
	}

	memset (&act, 0, sizeof(act));
	act.sa_handler = sigchld_hdl;
	if (sigaction(SIGCHLD, &act, 0)){
		perror("sigaction");
		return 1;
	}
//	signal(SIGCHLD, SIG_IGN);
	signal(SIGPIPE, SIG_IGN);
	while(1){
		printf("\nSelect your choice\n1. echo\n2. time\n3. exit\n");
		printf("Enter either 1,2 or 3");
		ch = fgets(inputbuff,1024,stdin);
		pipe(pipeFD);

		if(!strcmp(inputbuff,"1\n")){
			
			if( ( pid = fork() ) == -1)
   			{
           			 perror( "fork error: ");
           			 exit( 1);
   			}
  			else if(pid==0){
			close(pipeFD[0]);
			char pfd[10];
			sprintf(pfd, "%d", pipeFD[1]);
			execlp("xterm","xterm","-e","./echoclient",ipaddress,pfd,(char*)0);
			exit(0);
			}
			else{
			close(pipeFD[1]);
			M = 1;
			FD_ZERO(&setallfd);
			while(1){
				FD_SET(fileno(stdin), &setallfd);       
        			FD_SET(pipeFD[0], &setallfd);
				if (fileno(stdin) >= pipeFD[0])
					maxfd = fileno(stdin);
				else
					maxfd = pipeFD[0];                    
               
        			selectedvalue = select(maxfd+1, &setallfd, NULL, NULL, NULL);
        
        			if (M == 0)
					break;
			        if(FD_ISSET(pipeFD[0], &setallfd)) 
        			{	   
					if((rc=read(pipeFD[0],buffer,sizeof(buffer))) > 0)
					printf("from child client: %s\n",buffer);
				}
				if(FD_ISSET(fileno(stdin), &setallfd)) 
        			{	
					printf("do not enter anything here\n");   
					if (! fgets(buf, sizeof buf, stdin)) {
                    			    if (ferror(stdin)) 
                           			 perror("stdin");
					}
					
					
				}
			/*	if((rc=write(pipeFD1[1],"hello\0",6)) < 0)
				{
					break;
				}
		*/	}	
			//close(pipe1[0]);
			}
		}
		else if (!strcmp(inputbuff,"2\n")){
			if( ( pid = fork() ) == -1)
   			{
           			 perror( "fork error: ");
           			 exit( 1);
   			}
  			else if(pid==0){
			close(pipeFD[0]);
			char pfd[10];
			sprintf(pfd, "%d", pipeFD[1]);
			execlp("xterm","xterm","-e","./timeclient",ipaddress,pfd,(char*)0);
			exit(0);
			}
			else{
			close(pipeFD[1]);
			M = 1;
			FD_ZERO(&setallfd);
			while(1){
				FD_SET(fileno(stdin), &setallfd);       
        			FD_SET(pipeFD[0], &setallfd);
				if (fileno(stdin) >= pipeFD[0])
					maxfd = fileno(stdin);
				else
					maxfd = pipeFD[0];                    
               
        			selectedvalue = select(maxfd+1, &setallfd, NULL, NULL, NULL);
        
        			if (M == 0)
					break;

			        if(FD_ISSET(pipeFD[0], &setallfd)) 
        			{	   
					if((rc=read(pipeFD[0],buffer,sizeof(buffer))) > 0)
					printf("from child client: %s\n",buffer);
				}
				if(FD_ISSET(fileno(stdin), &setallfd)) 
        			{	   
					printf("do not enter anything here\n");
					if (! fgets(buf, sizeof buf, stdin)) {
                    			    if (ferror(stdin)) 
                           			 perror("stdin");
					}	
					
				}
				
		/*		if((rc=write(pipeFD1[1],"hello\0",6)) < 0)
				{
					break;
				}
	*/		}

			}
		}
		else if (!strcmp(inputbuff,"3\n"))
			break;
		else
			printf("wrong choice");
		
		
	}

return 0;
}
