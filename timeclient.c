#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <netdb.h>
#include <errno.h>
#define MAXLINE 1024

int main(int argc,char** argv)
{

	int sockfd;
	int len,rc,errno;
	int pipefd = atoi(argv[2]);
	struct sockaddr_in address;
	int result;
	char buf[MAXLINE],buf1[MAXLINE];

	printf("This is the client time program\n");
	sprintf(buf,"%s","\nclient time program started");
	write(pipefd, buf, sizeof(buf));


	sockfd = socket(PF_INET, SOCK_STREAM, 0);
	if (sockfd == -1)
	{
		perror("socket create failed.\n");
		sprintf(buf,"%s","\nsocket create failed.");
		write(pipefd, buf, sizeof(buf));
		return -1;
	}
	bzero(&address, sizeof(address));
	address.sin_family = AF_INET;
	address.sin_addr.s_addr = inet_addr(argv[1]);
	address.sin_port = htons(7186);
	len = sizeof(address);
	result = connect(sockfd, (struct sockaddr *)&address, len);
	if (result == -1)
	{
		perror("Error has occured");
		sprintf(buf,"%s","\nError has occured while connecting to the socket.");
		write(pipefd, buf, sizeof(buf));
		exit(-1);
	}

again:
	while ((rc = read(sockfd,buf1,MAXLINE)) > 0){
		write(1,buf1,rc);
	}
	if (rc == 0){
		printf("server terminated prematurely");
		sprintf(buf,"%s","\nserver terminated prematurely.");
		write(pipefd, buf, sizeof(buf));
	}
		
	if(rc < 0 && errno == EINTR)
		goto again;
	else if (rc < 0){ 
		printf("read error at time client");
		sprintf(buf,"%s","\nread error at time client.");
		write(pipefd, buf, sizeof(buf));
	}

	close(sockfd);
	return 0;
}
	

