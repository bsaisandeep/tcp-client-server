#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <netdb.h>
#include <errno.h>
#define MAXLINE 1024

int main(int argc,char **argv)
{
	int sockfd,result,maxfd;
	int pipefd = atoi(argv[2]);
	int len,rc,n,errno;
	struct sockaddr_in address;
	char buf[MAXLINE],buf1[MAXLINE],buf2[MAXLINE];
	fd_set setallfd;

	printf("This is the client echo program\n");
	sprintf(buf,"%s","\nclient echo program started");
	write(pipefd, buf, sizeof(buf));

	sockfd = socket(PF_INET, SOCK_STREAM, 0);
	if (sockfd == -1)
	{
		perror("socket create failed.\n");
		sprintf(buf,"%s","\nsocket create failed.");
		write(pipefd, buf, sizeof(buf));
		return -1;
	}
	bzero(&address, sizeof(address));
	address.sin_family = AF_INET;
	address.sin_addr.s_addr = inet_addr(argv[1]);
	address.sin_port = htons(7187);
	len = sizeof(address);
	result = connect(sockfd, (struct sockaddr *)&address, len);
	if (result == -1)
	{
		perror("Error has occured");
		sprintf(buf,"%s","\nError has occured while connecting to the socket.");
		write(pipefd, buf, sizeof(buf));
		exit(-1);
	}
	FD_ZERO(&setallfd);
	while(1){
	FD_SET(fileno(stdin), &setallfd);       
        FD_SET(sockfd, &setallfd);
	if (fileno(stdin) >= sockfd)
		maxfd = fileno(stdin);
	else
		maxfd = sockfd;                    
               
        errno = select(maxfd+1, &setallfd, NULL, NULL, NULL);
	if(FD_ISSET(fileno(stdin), &setallfd)){	
		if(fgets(buf2, sizeof(buf2), stdin) ==NULL){
			sprintf(buf,"%s","\nEnd of file encountered.");
			write(pipefd, buf, sizeof(buf));
			break; 
		}
		n = strlen(buf2);
		rc = write(sockfd, buf2, n);
		if ( rc < 0 )
			printf("error while writing");
	}
	if(FD_ISSET(sockfd, &setallfd)){
		if (read(sockfd, buf1, rc) == 0){
			printf("Error:server terminated prematurely");
			sprintf(buf,"%s","Error:server terminated prematurely\n");
			write(pipefd, buf, sizeof(buf));
			break;}
		write(1,buf1,rc);
	}
	}
	
		
	if(n < 0 && errno == EINTR){
		printf("Interrupt error occured");sleep(2);}	
	//goto again;
	else if (n < 0) 
		printf("echoclient: read error");

	close(sockfd);
	return 0;
}
	

