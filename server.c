#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <time.h>
#include <pthread.h>
#include <errno.h>
#include <fcntl.h>
#include <signal.h>
#include <wait.h>
#define MAXLINE 1024
#define ECHO_SERVER_PORT		7187
#define TIME_SERVER_PORT		7186

void setnonblocking(int sock)
{
	int opts;

	opts = fcntl(sock,F_GETFL,0);
	if (opts < 0) {
		perror("fcntl(F_GETFL)");
		exit(EXIT_FAILURE);
	}
	opts = (opts | O_NONBLOCK);
	if (fcntl(sock,F_SETFL,opts) < 0) {
		perror("fcntl(F_SETFL)");
		exit(EXIT_FAILURE);
	}
	return;
}

void setblocking(int sock)
{
	int opts;

	opts = fcntl(sock,F_GETFL,0);
	if (opts < 0) {
		perror("fcntl(F_GETFL)");
		exit(EXIT_FAILURE);
	}
	opts = (opts & ~O_NONBLOCK);
	if (fcntl(sock,F_SETFL,opts) < 0) {
		perror("fcntl(F_SETFL)");
		exit(EXIT_FAILURE);
	}
	return;

	} 

static void sigchld_hdl (int sig)
{
	pid_t pid;
	/* Wait for all dead processes.
	 * We use a non-blocking call to be sure this signal handler will not
	 * block if a child was cleaned up in another part of the program. */
	while ((pid = waitpid(-1, NULL, WNOHANG)) > 0) {
		printf("CHILD %d terminated\n",pid);
	}
}

static void sigpipe_hdl (int sig)
{
		printf("CHILD pipe terminated\n");
}


void *displaytime(void *argv)
{
	time_t ticks;
	int rc;
	int sock = *(int *)argv;
	pthread_t child;	
	signal(SIGPIPE, SIG_IGN);
	while(1){
		ticks=time(NULL);
		char* ti=ctime(&ticks);
	        rc=write(sock,ti,strlen(ti));
		if (rc < 0)
		break;
		printf("time is %s", ti);
		sleep(5);
		}
	

	close(sock);

	child = pthread_self();
	pthread_detach(child);
}

void *echomsg(void *argv)
{
	int rc,n,errno;
	char buf[MAXLINE];	
	int sock = *(int *)argv;
	pthread_t child;

again:		while(((n=read(sock,buf,MAXLINE))> 0)){
//			printf("%s",buf);
			rc = write(sock,buf,n);
		}
		if (n < 0 && errno == EINTR)
			goto again;
		else if (n < 0)
			perror("read error at socket");

	close(sock);
	child = pthread_self();
	pthread_detach(child);
}

int main(int argc,char **arg)
{
pthread_t mythread[MAXLINE];


	int server_sockfd, server_sockfd1, time_sockfd, echo_sockfd;
	int rc,server_len;
	int count=0,errno, maxfd;
	unsigned client_len;
	struct sockaddr_in server_address;
	struct sockaddr_in time_address, echo_address;

	fd_set setallfd;
	char flag = '1';

	bzero(&server_address, sizeof(server_address));	
	server_sockfd = socket(AF_INET, SOCK_STREAM, 0);
	if (server_sockfd < 0 ) { 
 	   printf("Server: Error Opening socket: Error");
	} 
	server_address.sin_family = AF_INET;
	server_address.sin_addr.s_addr = htonl(INADDR_ANY);
	server_address.sin_port = htons(ECHO_SERVER_PORT);
	server_len = sizeof(server_address);
	setsockopt(server_sockfd, SOL_SOCKET, SO_REUSEADDR, &flag, sizeof(flag));

	setnonblocking(server_sockfd);
	rc = bind(server_sockfd, (struct sockaddr *) &server_address, server_len);
	if (rc < 0 ) { 
 	   printf("Server: Bind error");
	} 

	rc = listen(server_sockfd, 5);
	if (rc < 0 ) { 
 	   printf("Server: listen error");
	} 

	bzero(&server_address, sizeof(server_address));	
	server_sockfd1 = socket(AF_INET, SOCK_STREAM, 0);
	if (server_sockfd1 < 0 ) { 
 	   printf("Server: Error Opening socket: Error");
	} 
	server_address.sin_family = AF_INET;
	server_address.sin_addr.s_addr = htons(INADDR_ANY);
	server_address.sin_port = htons(TIME_SERVER_PORT);
	server_len = sizeof(server_address);

	setsockopt(server_sockfd1, SOL_SOCKET, SO_REUSEADDR, &flag, sizeof(flag));
	setnonblocking(server_sockfd1);
	rc = bind(server_sockfd1, (struct sockaddr *) &server_address, server_len);
	if (rc < 0 ) { 
 	   printf("Server: Bind error");
	} 

	printf("RC from listen = %d\n",rc);


	rc = listen(server_sockfd1, 5);
	if (rc < 0 ) { 
 	   printf("Server: listen error");
	} 
	
	signal(SIGCHLD, sigchld_hdl);	
	FD_ZERO(&setallfd);

	while(1){
		
		count++;
		FD_SET(server_sockfd, &setallfd);       
        	FD_SET(server_sockfd1, &setallfd);   
		
		if (server_sockfd >= server_sockfd1)
		maxfd = server_sockfd;
		else
		maxfd = server_sockfd1;                    
               
        	errno = select(maxfd+1, &setallfd, NULL, NULL, NULL);
        
        
	        if(FD_ISSET(server_sockfd, &setallfd)) 
        	{	
		client_len = sizeof(echo_address);
		echo_sockfd = accept(server_sockfd, (struct sockaddr *) &echo_address, &client_len);
		printf("after accept()... client_sockfd = %d\n", echo_sockfd);
		setblocking(echo_sockfd);	
		errno = pthread_create(&mythread[count], NULL, &echomsg, (void *)&echo_sockfd);
		if(errno)
			perror("error in calling pthread create of echo");
		}



		if(FD_ISSET(server_sockfd1, &setallfd)) 
        	{	
		client_len = sizeof(time_address);
		time_sockfd = accept(server_sockfd1, (struct sockaddr *) &time_address, &client_len);
		printf("after accept()... client_sockfd = %d\n", time_sockfd);
		setblocking(time_sockfd);	
		errno = pthread_create(&mythread[count], NULL, &displaytime, (void *)&time_sockfd);
		if(errno)
			perror("error in calling pthread create of echo");
		}


	}
	close(server_sockfd);
	close(server_sockfd1);
	return 0;
}
































































































 
